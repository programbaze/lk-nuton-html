jQuery(function() {
    jQuery(document).ready(function() {
        if( jQuery(window).width() < 1064){
            jQuery('.js__news').slick({
                    dots: false,
                    slidesToShow: 2,
                    infinite: true,
                    autoplay: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 767,
                            settings: {
                                slidesToShow: 1,
                            }
                        }
                    ]

                }
            );
        }
    });

});
