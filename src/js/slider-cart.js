jQuery(function() {
    jQuery('.js__slider-cart').slick({
            dots: true,
            slidesToShow: 1,
            dotsClass: 'slider-cart__dots',
            arrows: false
        }
    );
});
