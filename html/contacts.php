<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Контакты</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Контакты</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Контакты</h1>
                    </div>
                  </div>

                    <div class="contacts">
                        <div class="contacts__inner">

                            <div class="contacts__panel">

                                <span class="switch-unit">
                                    <span class="switch-unit__title">Брянск</span>
                                    <span class="link-arrow switch-unit__link js__active">Перейти</span>
                                    <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                                </span>

                                <span class="switch-unit">
                                    <span class="switch-unit__title">Калуга</span>
                                    <span class="link-arrow switch-unit__link js__active">Перейти</span>
                                    <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                                </span>
                                <span class="switch-unit">
                                    <span class="switch-unit__title">Орел</span>
                                    <span class="link-arrow switch-unit__link js__active">Перейти</span>
                                    <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                                </span>

                                <span class="switch-unit">
                                    <span class="switch-unit__title">Смоленске</span>
                                    <span class="link-arrow switch-unit__link js__active">Перейти</span>
                                    <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                                </span>

                                <span class="switch-unit">
                                    <span class="switch-unit__title">Тула</span>
                                    <span class="link-arrow switch-unit__link js__active">Перейти</span>
                                    <img src="/f/i/icons/location-pointer.svg" class="switch-unit__point-img">
                                </span>

                            </div>

                            <div class="contacts__main">
                                <div class="contacts__title">Адреса магазинов и пунктов выдачи в Брянске</div>

                                <div class="contacts__desc">В нашем магазине вы можете приобрести весь спектр товаров для электромонтажа и монтажа отопления. В случае если вы на нашли интересующий Вас товар, позвоните по контактному телефону 8(4832) 400-200 и мы предложим Вам товар с аналогичными характеристиками.</div>
                                <div class="contacts__map">
                                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae05b7eb44829114511fb1ec523a79d2c73ad320bd3738df05076e9cd55f08d33&amp;width=100%25&amp;height=600&amp;lang=ru_RU&amp;scroll=false"></script>
                                </div>


                                <div class="contacts__shops">
                                    <div class="contacts__item-shop">
                                        <div class="contacts__shop-wrap-img">
                                            <img src="/f/i/store/faza1.jpg" alt="">
                                        </div>
                                        <div class="contacts__shop-info">
                                            <div class="contacts__shop-title">Брянск</div>
                                            <div class="contacts__shop-adress">Брянская область д. Добрунь, ул. С.А. Халаева, 74</div>
                                            <a class="contacts__shop-phone" href="tel:84832400200">8 (4832) 400 200</a>
                                            <a class="contacts__shop-mail" rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                            <div class="contacts__shop-clock">Время работы: Пн-Пт: 8:30-18:00; Сб: 10:00 - 16:00; Вс: Выходной</div>
                                        </div>
                                    </div>
                                    <div class="contacts__item-shop">
                                        <div class="contacts__shop-wrap-img">
                                            <img src="/f/i/store/nofoto.jpg" alt="">
                                        </div>
                                        <div class="contacts__shop-info">
                                            <div class="contacts__shop-title">Калуга</div>
                                            <div class="contacts__shop-adress">Калуга, улица Болдина, 87к1</div>
                                            <a class="contacts__shop-phone" href="tel:84832400200">8 (4832) 400 200</a>
                                            <a class="contacts__shop-mail" rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                            <div class="contacts__shop-clock">Время работы: 9:00-18:00</div>
                                        </div>
                                    </div>
                                    <div class="contacts__item-shop">
                                        <div class="contacts__shop-wrap-img">
                                            <img src="/f/i/store/nofoto.jpg" alt="">
                                        </div>
                                        <div class="contacts__shop-info">
                                            <div class="contacts__shop-title">Орел</div>
                                            <div class="contacts__shop-adress">Орел, Карачевское шоссе, 79</div>
                                            <a class="contacts__shop-phone" href="tel:84832400200">8 (4832) 400 200</a>
                                            <a class="contacts__shop-mail" rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                            <div class="contacts__shop-clock">Время работы: Пн-пт: 8:30-17:30 Cб-Вск: выходной</div>
                                        </div>
                                    </div>
                                    <div class="contacts__item-shop">
                                        <div class="contacts__shop-wrap-img">
                                            <img src="/f/i/store/nofoto.jpg" alt="">
                                        </div>
                                        <div class="contacts__shop-info">
                                            <div class="contacts__shop-title">Смоленск</div>
                                            <div class="contacts__shop-adress">Смоленск, пос. Тихвинка, 46а</div>
                                            <a class="contacts__shop-phone" href="tel:88007005480">8 800 700 54 80</a>
                                            <a class="contacts__shop-mail" rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                            <div class="contacts__shop-clock">Время работы: 9:00-18:00</div>
                                        </div>
                                    </div>
                                    <div class="contacts__item-shop">
                                        <div class="contacts__shop-wrap-img">
                                            <img src="/f/i/store/tula.jpg" alt="">
                                        </div>
                                        <div class="contacts__shop-info">
                                            <div class="contacts__shop-title">Тула</div>
                                            <div class="contacts__shop-adress">Тула, Щекинское шоссе 26Б</div>
                                            <a class="contacts__shop-phone" href="tel:+74872700404">+7 (4872) 700 404</a>
                                            <a class="contacts__shop-mail" rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>
                                            <div class="contacts__shop-clock">Время работы: Пн-пт: 9:00-19:00&nbsp;Сб: 9.00-17.00</div>
                                        </div>
                                    </div>
                                </div>

<!--                                <div class="contacts__shops">-->
<!--                                    <div class="contacts__item-shop">-->
<!--                                        <div class="contacts__shop-wrap-img">-->
<!--                                            <img src="/f/i/store/faza1.jpg" alt="">-->
<!--                                        </div>-->
<!--                                        <div class="contacts__shop-info">-->
<!--                                            <div class="contacts__shop-title">Фаза на объездной</div>-->
<!--                                            <div class="contacts__shop-adress">Брянск, Брянская область д. Добрунь, ул. С.А. Халаева, 74</div>-->
<!--                                            <a class="contacts__shop-phone" href="tel:+74832400-200">+7(4832)400-200</a>-->
<!--                                            <a class="contacts__shop-mail rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>-->
<!--                                            <div class="contacts__shop-clock">Время работы: Пн-Пт: 9:00-20:00; Сб-Вск: 10:00-16:00</div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="contacts__item-shop">-->
<!--                                        <div class="contacts__shop-wrap-img">-->
<!--                                            <img src="/f/i/store/faza2.jpg" alt="">-->
<!--                                        </div>-->
<!--                                        <div class="contacts__shop-info">-->
<!--                                            <div class="contacts__shop-title">Фаза в Линии 3</div>-->
<!--                                            <div class="contacts__shop-adress">Брянск, Россия, улица Ульянова, 3</div>-->
<!--                                            <a class="contacts__shop-phone" href="tel:+74832400-200">+7(4832)400-200</a>-->
<!--                                            <a class="contacts__shop-mail rel="nofollow" href="mailto:info@faza.center">info@faza.center</a>-->
<!--                                            <div class="contacts__shop-clock">Время работы: Пн-Пт: 9:00-20:00; Сб-Вск: 10:00-16:00</div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <div class="contacts__feedback">
                                    <div class="red-btn footer__btn">Обратная связь</div>
                                </div>
                            </div>
                        </div>
                    </div>







                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
