<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Отгрузки</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Ваши отгрузки</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Ваши отгрузки</h1>
                    </div>
                  </div>

                  <div class="list-status">
                    <div class="list-status__inner">
                      <div class="status status_new"><span class="status__text">Новый заказ</span></div>
                      <div class="status status_equipment"><span class="status__text">Заказ в комплектации</span></div>
                      <div class="status status_staffed"><span class="status__text">Заказ укамплектован</span></div>
                      <div class="status status_delivery"><span class="status__text">Заказ в доставке</span></div>
                      <div class="status status_done"><span class="status__text">Заказ выполнен</span></div>
                      <div class="status status_canceled"><span class="status__text">Заказ отменен</span></div>
                    </div>
                  </div>

                  <div class="choice">
                    <div class="choice__inner">

                      <div class='shipments'>
                          <div class="shipments__inner">
                              <div class="shipments__head">
                                  <div class="shipments__head-title">№</div>
                                  <div class="shipments__head-title">Дата</div>
                                  <div class="shipments__head-title">Наименование  <span class='shipments__sorting shipments__sorting_active'></div>
                                  <div class="shipments__head-title">Ваша цена <span class='shipments__sorting'></span></div>
                                  <div class="shipments__head-title">Статус</div>
                                  <div class="shipments__head-title"></div>
                                  <div class="shipments__head-title"></div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">1</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_new"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn"></div></div>
                                  </div>
                                  <div class="accordion__content shipments__item-content">
                                      <div class="shipment-table">
                                          <div class="shipment-table__row shipment-table__row_head">
                                              <div class="shipment-table__coll">Фото</div>
                                              <div class="shipment-table__coll">Артикул</div>
                                              <div class="shipment-table__coll">Наименование</div>
                                              <div class="shipment-table__coll">Бренд</div>
                                              <div class="shipment-table__coll">Доступность</div>
                                              <div class="shipment-table__coll">Упаковка</div>
                                              <div class="shipment-table__coll">Ваша цена</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">2</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_equipment"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn open"></div></div>
                                  </div>
                                  <div class="accordion__content shipments__item-content show">
                                      <div class="shipment-table">
                                          <div class="shipment-table__row shipment-table__row_head">
                                              <div class="shipment-table__coll">Фото</div>
                                              <div class="shipment-table__coll">Артикул</div>
                                              <div class="shipment-table__coll">Наименование</div>
                                              <div class="shipment-table__coll">Бренд</div>
                                              <div class="shipment-table__coll">Доступность</div>
                                              <div class="shipment-table__coll">Упаковка</div>
                                              <div class="shipment-table__coll">Ваша цена</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">3</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_staffed"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn"></div></div>

                                      <div class="shipments__modal" style="position: absolute; top: 60px; right: 60px;">
                                          <div class="shipments__modal-item">
                                              <a href="#" class="img-btn img-btn_check">Добавить счет</a>
                                          </div>
                                          <div class="shipments__modal-item">
                                              <a href="#" class="img-btn img-btn_print">Печать</a>
                                          </div>
                                          <div class="shipments__modal-item">
                                              <a href="#" class="img-btn img-btn_save">Скачать</a>
                                          </div>
                                      </div>

                                  </div>
                                  <div class="accordion__content shipments__item-content show">
                                      <div class="shipment-table">
                                          <div class="shipment-table__row shipment-table__row_head">
                                              <div class="shipment-table__coll">Фото</div>
                                              <div class="shipment-table__coll">Артикул</div>
                                              <div class="shipment-table__coll">Наименование</div>
                                              <div class="shipment-table__coll">Бренд</div>
                                              <div class="shipment-table__coll">Доступность</div>
                                              <div class="shipment-table__coll">Упаковка</div>
                                              <div class="shipment-table__coll">Ваша цена</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">4</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_delivery"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn open"></div></div>
                                  </div>
                                  <div class="accordion__content shipments__item-content show">
                                      <!-- Adaptive block shipments (show 1319px)-->
                                      <div class="prod-small">
                                          <div class="prod-small__inner">
                                              <!--  Item shipments-->
                                              <div class="prod-small__item">
                                                  <div class="prod-small__left">
                                                      <div class="prod-small__wrap-img">
                                                          <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__right">
                                                      <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                                      <div class="prod-small__characteristic">
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Артикул</div>
                                                              <div class="prod-small__characteristic-value">N23589GT2</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Бренд</div>
                                                              <div class="prod-small__characteristic-value">Gauss</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Доступность</div>
                                                              <div class="prod-small__characteristic-value">648 шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Упаковка</div>
                                                              <div class="prod-small__characteristic-value">100шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Ваша цена</div>
                                                              <div class="prod-small__characteristic-value">152 р.</div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <!--  // Item shipments-->
                                              <!--  Item shipments-->
                                              <div class="prod-small__item">
                                                  <div class="prod-small__left">
                                                      <div class="prod-small__wrap-img">
                                                          <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__right">
                                                      <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                                      <div class="prod-small__characteristic">
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Артикул</div>
                                                              <div class="prod-small__characteristic-value">N23589GT2</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Бренд</div>
                                                              <div class="prod-small__characteristic-value">Gauss</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Доступность</div>
                                                              <div class="prod-small__characteristic-value">648 шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Упаковка</div>
                                                              <div class="prod-small__characteristic-value">100шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Ваша цена</div>
                                                              <div class="prod-small__characteristic-value">152 р.</div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <!--  // Item shipments-->
                                              <!--  Item shipments-->
                                              <div class="prod-small__item">
                                                  <div class="prod-small__left">
                                                      <div class="prod-small__wrap-img">
                                                          <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__right">
                                                      <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                                      <div class="prod-small__characteristic">
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Артикул</div>
                                                              <div class="prod-small__characteristic-value">N23589GT2</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Бренд</div>
                                                              <div class="prod-small__characteristic-value">Gauss</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Доступность</div>
                                                              <div class="prod-small__characteristic-value">648 шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Упаковка</div>
                                                              <div class="prod-small__characteristic-value">100шт.</div>
                                                          </div>
                                                          <div class="prod-small__item-characteristic">
                                                              <div class="prod-small__characteristic-name">Ваша цена</div>
                                                              <div class="prod-small__characteristic-value">152 р.</div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <!--  // Item shipments-->
                                          </div>
                                      </div>
                                      <!-- // Adaptive block shipments (show 1319px)-->
                                  </div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">5</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_done"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn"></div></div>
                                  </div>
                                  <div class="accordion__content shipments__item-content">
                                      <div class="shipment-table">
                                          <div class="shipment-table__row shipment-table__row_head">
                                              <div class="shipment-table__coll">Фото</div>
                                              <div class="shipment-table__coll">Артикул</div>
                                              <div class="shipment-table__coll">Наименование</div>
                                              <div class="shipment-table__coll">Бренд</div>
                                              <div class="shipment-table__coll">Доступность</div>
                                              <div class="shipment-table__coll">Упаковка</div>
                                              <div class="shipment-table__coll">Ваша цена</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="accordion shipments__item">
                                  <div class="accordion__head shipments__item-head">
                                      <div class="shipments__item-title">6</div>
                                      <div class="shipments__item-title">21.07.2021</div>
                                      <div class="shipments__item-title">Отгрузка №1 - 4 товара</div>
                                      <div class="shipments__item-title">2 562 руб.</div>
                                      <div class="shipments__item-title"><div class="status status_canceled"></div></div>
                                      <div class="shipments__item-title"><div class="shipments__setting"></div></div>
                                      <div class="shipments__item-title"><div class="accordion__btn"></div></div>
                                  </div>
                                  <div class="accordion__content shipments__item-content">
                                      <div class="shipment-table">
                                          <div class="shipment-table__row shipment-table__row_head">
                                              <div class="shipment-table__coll">Фото</div>
                                              <div class="shipment-table__coll">Артикул</div>
                                              <div class="shipment-table__coll">Наименование</div>
                                              <div class="shipment-table__coll">Бренд</div>
                                              <div class="shipment-table__coll">Доступность</div>
                                              <div class="shipment-table__coll">Упаковка</div>
                                              <div class="shipment-table__coll">Ваша цена</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                          <div class="shipment-table__row">
                                              <div class="shipment-table__coll"><img class="shipments__img" src="/f/i/slider-cart/1.jpg"></div>
                                              <div class="shipment-table__coll">N23589GT2</div>
                                              <div class="shipment-table__coll">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                              <div class="shipment-table__coll">Gauss</div>
                                              <div class="shipment-table__coll">648 шт.</div>
                                              <div class="shipment-table__coll">100шт.</div>
                                              <div class="shipment-table__coll">52 р.</div>
                                          </div>
                                      </div>
                                  </div>
                              </div>


                          </div>



                      </div>


                    </div>
                  </div>



                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
