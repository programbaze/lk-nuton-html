<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Список</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/main.min.css"" rel="stylesheet">
    <style>

    </style>
</head>
<body>
<div style="min-height: 800px; padding: 50px;">
  <ul>
    <li><a target="_blank" href="/html/home.php">Главная</a></li>
    <li><a target="_blank" href="/html/login.php">Вход</a></li>
    <li><a target="_blank" href="/html/registration.php">Страница регистрации</a></li>
    <li><a target="_blank" href="/html/catalog.php">Каталог 1 уровень</a></li>
    <li><a target="_blank" href="/html/catalog2.php">Каталог 2 уровень</a></li>
    <li><a target="_blank" href="/html/cart.php">Карточка товара</a></li>
    <li><a target="_blank" href="/html/basket.php">Корзина</a></li>
    <li><a target="_blank" href="/html/shipments.php">Ваши отгрузки</a></li>
    <li><a target="_blank" href="/html/profile.php">Профиль</a></li>
    <li><a target="_blank" href="/html/statistics.php">Статистика</a></li>
    <li><a target="_blank" href="/html/check.php">Счет (ЭлектроСтрой)</a></li>
    <li><a target="_blank" href="/html/check2.php">Счет (Архипченко)</a></li>
    <li><a target="_blank" href="/html/favorites.php">Избранное</a></li>
    <li><a target="_blank" href="/html/article.php">Статья</a></li>
    <li><a target="_blank" href="/html/acts.php">Акты сверки</a></li>
    <li><a target="_blank" href="/html/act-reconciliation.php">Акт сверки (для pdf) (ЭлектроСтрой)</a></li>
    <li><a target="_blank" href="/html/act-reconciliation2.php">Акт сверки (для pdf) (Архипченко)</a></li>
    <li><a target="_blank" href="/html/contacts.php">Офисы продаж</a></li>
    <li><a target="_blank" href="/html/support.php">Поддержка</a></li>
    <li><a target="_blank" href="/html/about.php">О нас</a></li>
    <li><a target="_blank" href="/html/notification.php">Уведомления</a></li>
    <li><a target="_blank" href="/html/news.php">Новости</a></li>
    <li><a target="_blank" href="/html/mail.html">Email - универсальный</a></li>
    <li><a target="_blank" href="/html/mail-change-password.html">Email - смена пароля</a></li>
    <li><a target="_blank" href="/html/mail-news.html">Email - новости</a></li>
  </ul>
</div>
</body>
</html>
