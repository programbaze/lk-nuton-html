<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Каталог</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Каталог</a></li>
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Источники света</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Светодиодные лампы</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Светодиодные лампы (LED)</h1>
                      <div class="title__amount">5148</div>
                    </div>
                  </div>


                  <div class="choice">
                    <div class="choice__inner">

                      <div class="choice__setting-product">
                        <div class="choice__wrap-select-brand">
                          <select class="form-select choice__select" aria-label="Выбрать производителя">
                            <option>Выбрать производителя</option>
                            <option value="1">Gauss</option>
                            <option value="2">Varta</option>
                            <option value="3">Osram</option>
                            <option value="3">Philips</option>
                          </select>
                        </div>
                        <div class="choice__form-check form-check">
                          <input class="form-check-input" type="checkbox" value="" id="checkBalance" checked>
                          <label class="form-check-label"  for="checkBalance">Только с остатками</label>
                        </div>
                        <div class="choice__form-check form-check">
                          <input class="form-check-input" type="checkbox" value="" id="checkSale" >
                          <label class="form-check-label"  for="checkSale">Позиции со скидкой</label>
                        </div>
                      </div>

                      <div class='product'>

                          <div class="product__modal" style="position: absolute; top: 50px; right: 60px;">
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting1" >
                                  <label class="form-check-label"  for="setting1">Кратность упаковки</label>
                              </div>
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting2" >
                                  <label class="form-check-label"  for="setting2">Производитель</label>
                              </div>
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting3" >
                                  <label class="form-check-label"  for="setting3">Цоколь</label>
                              </div>
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting4" >
                                  <label class="form-check-label"  for="setting4">Цветовая температура, K</label>
                              </div>
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting5" >
                                  <label class="form-check-label"  for="setting5">Степень защиты, IP</label>
                              </div>
                              <div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting6" >
                                  <label class="form-check-label"  for="setting6">Световой поток, Лм</label>
                              </div><div class="product__modal-item">
                                  <input class="form-check-input" type="checkbox" value="" id="setting7" >
                                  <label class="form-check-label"  for="setting7">Мощность, Вт</label>
                              </div>
                          </div>


                        <table class='product__table'>

                          <tr class='product__head product__row'>
                            <td class='product__head-col product__col'>Фото</td>
                            <td class='product__head-col product__col'>Артикул</td>
                            <td class='product__head-col product__col'>Наименование <span class='product__sorting product__sorting_active'></span></td>
                            <td class='product__head-col product__col'>Бренд <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Доступность <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Упаковка <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Ваша цена <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Кол-во</td>
                            <td class='product__head-col product__col'></td>
                            <td class='product__head-col product__col product__tx-end'><span class='product__setting'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                    <button class="btn-minus"></button>
                                    <input type="text" class="amount__coll" value="99">
                                    <button class="btn-plus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart active'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart active'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart active'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart active'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart active'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт. <span class="product__compact">(1600 шт.)</span></td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                  <div class="amount__inner">
                                      <button class="btn-minus"></button>
                                      <input type="text" class="amount__coll" value="99">
                                      <button class="btn-plus"></button>
                                  </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><div class='btn-heart'></div></td>
                            <td class='product__col product__tx-end'><span class='btn-cart active'></span></td>
                          </tr>

                        </table>

                          <!-- Adaptive block product (show 1319px)-->
                          <div class="prod-small">
                              <div class="prod-small__inner">
                                  <div class="prod-small__setting">
                                      <div class="product__setting"></div>
                                  </div>
                                <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт. <span class="prod-small__compact">(1600 шт.)</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-minus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-plus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-cart active"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/1.jpg">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт. <span class="prod-small__compact">(1600 шт.)</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-minus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-plus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-cart"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт. <span class="prod-small__compact">(1600 шт.)</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-minus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-plus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-cart"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/1.jpg">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт. <span class="prod-small__compact">(1600 шт.)</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-minus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-plus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-cart"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт. <span class="prod-small__compact">(1600 шт.)</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-minus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-plus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-cart"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                              </div>
                          </div>
                          <!-- // Adaptive block product (show 1319px)-->

                        <div class="prodnav">
                            <div class="prodnav__inner">
                                <div class="prodnav__item prodnav__item_active">1</div>
                                <div class="prodnav__item">2</div>
                                <div class="prodnav__item">3</div>
                                <div class="prodnav__item">4</div>
                                <div class="prodnav__item">5</div>
                                <div class="prodnav__item">...</div>
                                <div class="prodnav__item">12</div>
                            </div>
                        </div>

                      </div>


                    </div>
                  </div>



                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
