<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Вход</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="/f/css/bootstrap.min.css" rel="stylesheet">
  <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <main class="main">

          <div class="enter">
            <div class="enter__inner enter__inner_auth">

              <form method="POST" action="http://lk.nuton-electro.ru/login">
                <input type="hidden" name="_token" value="Dw5e0Twk1T8azWvpR6DcejPAOSyNUm9qFXZWAqEw">                <div class="form-group mb-3"><h4>Добро пожаловать в личный кабинет <nobr>ГК "Ньютон электро"</nobr></h4></div>

                <div class="form-group mb-3">
                  <label for="email">E-Mail Address</label>
                  <input id="email" type="email" class="form-control invalid" name="email" value="" required="" autocomplete="email" autofocus="">
                    <div class="invalid-feedback" style="display: block">
                        Не верно
                    </div>
                </div>

                <div class="form-group mb-3">
                  <label for="password">Пароль</label>
                  <input id="password" type="password" class="form-control invalid" name="password" required="" autocomplete="current-password">
                    <div class="invalid-feedback" style="display: block">
                        Не верно
                    </div>
                </div>

                <div class="form-check mb-3">
                  <input class="form-check-input" type="checkbox" name="remember" id="remember">
                  <label class="form-check-label" for="remember">
                    Запомнить меня
                  </label>
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    <button type="submit" class="red-btn col-md-12">
                      Войти
                    </button>
                  </div>

                  <div class="col-md-5">
                    <button type="submit" class="red-btn col-md-12">
                      Зарегистрироваться
                    </button>
                  </div>
                </div>


              </form>

            </div>

          </div>

        </main>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>
</body>
</html>
