<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style type="text/css">
        * {font-family: arial;font-size: 14px; line-height: 18px;}
        table {margin: 0 0 20px 0;width: 100%;border-collapse: collapse;border-spacing: 0;}
        table th {padding: 5px;font-weight: bold;}
        table td {padding: 5px;}
        .header {margin: 20px 0 20px 0}
        .header td{ padding: 0 15px; text-align: center; font-size: 14px; line-height: 18px;}
        h1 {margin: 0 0 10px 0;padding: 10px 0;font-weight: bold;font-size: 20px;}

        .details td {padding: 5px;border: 2px solid #000000;font-size: 14px; line-height: 18px;vertical-align: top;}

        .contract th {padding: 3px 0;vertical-align: top;text-align: left;font-size: 13px;line-height: 15px;}
        .contract td {padding: 3px 0;}

        .sign {position: relative;}
        .sign table {width: 90%;}
        .sign th {}
        .sign td {border-bottom: 1px solid #000;font-size: 12px;}
    </style>
</head>
<body>

<table class="header">
    <tr>
        <td>
            <h1>Акт сверки взаимных расчетов № UT-98 от 15 декабря 2023 г</h1>
        </td>
    </tr>
    <tr>
        <td>
            за Январь 2023 г. - Октябрь 2023 г.<br/>
            между ИП Архипченко Павел Николаевич и АО "ЭР-Телеком Холдинг"
        </td>
    </tr>
</table>

<table class="details">

    <tr>
        <td colspan="9" style="border: none; padding: 0 0 20px 0; text-indent: 20px;">
            Мы, нижеподписавшиеся, Руководитель ИП Архипченко Павел Николаевич, с одной стороны, и АО "ЭР-Телеком Холдинг", с другой стороны, составили настоящий акт
            сверки в том, что:
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: none; padding: 0 0 20px 0; text-indent: 20px;">
            1. В период с 1 января 2023 г. по 31 октября 2023 г. были осуществлены следующие расчеты:
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: none; padding: 0 0 5px 0;">
            в валюте руб
        </td>
    </tr>

    <tr>
        <td rowspan="3" align="center" style="vertical-align: middle">Дата</td>
        <td rowspan="3" align="center" style="vertical-align: middle">Документ</td>
        <td rowspan="3" align="center" style="vertical-align: middle">Валюта<br/>документа</td>
        <td colspan="6" align="center">по данным</td>
    </tr>
    <tr>
        <td colspan="3" align="center">ИП Шестаков Андрей Анатольевич</td>
        <td colspan="3" align="center">АО "ЭР-Телеком Холдинг"</td>
    </tr>
    <tr>
        <td align="center">Сумма<br/>документа</td>
        <td align="center">Дебет</td>
        <td align="center">Кредит</td>
        <td align="center">Сумма<br/>документа</td>
        <td align="center">Дебет</td>
        <td align="center">Кредит</td>
    </tr>
    <tr>
        <td style="border-right: none;"></td>
        <td colspan="3" style="border-left: none;"><strong>Сальдо начальное</strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong></strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong>-</strong></td>
    </tr>
    <tr>
        <td>24.08.2023</td>
        <td>Акт выполненных работ №UT-291 от 24.08.2023</td>
        <td align="center">руб.</td>
        <td align="right">67 600,00</td>
        <td align="right">67 600,00</td>
        <td align="right">-</td>
        <td align="right">67 600,00</td>
        <td align="right">-</td>
        <td align="right">67 600,00</td>
    </tr>
    <tr>
        <td>23.10.2023</td>
        <td>Платежное поручение №213276 от 23.10.2023</td>
        <td align="center">руб.</td>
        <td align="right">67 600,00</td>
        <td align="right">-</td>
        <td align="right">67 600,00</td>
        <td align="right">67 600,00</td>
        <td align="right">67 600,00</td>
        <td align="right">-</td>
    </tr>
    <tr>
        <td style="border-right: none;"></td>
        <td colspan="3" style="border-left: none;"><strong>Обороты за период</strong></td>
        <td align="right"><strong>67 600,00</strong></td>
        <td align="right"><strong>67 600,00</strong></td>
        <td align="right"><strong></strong></td>
        <td align="right"><strong>67 600,00</strong></td>
        <td align="right"><strong>67 600,00</strong></td>
    </tr>
    <tr>
        <td style="border-right: none;"></td>
        <td colspan="3" style="border-left: none;"><strong>Сальдо конечное</strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong></strong></td>
        <td align="right"><strong>-</strong></td>
        <td align="right"><strong>-</strong></td>
    </tr>

</table>


<div class="sign">
    <table width="100%">
        <tr>
            <td width="30%" align="center" style="vertical-align: top; 20px; border-bottom: none; font-size: 14px; line-height: 18px;">
                ИП Архипченко Павел Николаевич, ИНН
                323401752808, 241037, Брянская обл.
                г. Брянск ул. Авиационная д. 5 кв. 48
            </td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="vertical-align: top; border-bottom: none; font-size: 14px; line-height: 18px;">
                АО "ЭР-Телеком Холдинг", ИНН 5902202276, 614066,
                г Пермь, Космонавтов Шоссе, д. 111 и, к. 2

            </td>
        </tr>

        <tr>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 20px;"><strong>Руководитель</strong></td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 20px;"></td>
        </tr>
        <tr>
            <td  width="30%" align="center" style="border-bottom: none; font-size: 12px;">должность</td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="border-bottom: none; font-size: 12px;">должность</td>
        </tr>

        <tr>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 50px;">
                <span style="display: inline-block; position: relative;">
                    <img style="display: inline-block; position: absolute; bottom: -40px; left: -60px;" width="90px" src="/f/i/signature/arhipchenko.png">
                    <img style="display: inline-block; position: absolute; bottom: -80px; left: -125px;" width="200px" src="/f/i/signature/arhipchenko-stamp.png">
                </span>
            </td>
            <td width="5%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 50px;"></td>
        </tr>
        <tr>
            <td  width="30%" align="center" style="border-bottom: none; font-size: 12px;">подпись</td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="border-bottom: none; font-size: 12px;">подпись</td>
        </tr>

        <tr>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 30px;"><strong>Архипченко П. Н.</strong></td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="vertical-align: top; padding-top: 30px;"></td>
        </tr>
        <tr>
            <td  width="30%" align="center" style="border-bottom: none; font-size: 12px;">расшифровка</td>
            <td width="20%" style="border-bottom: none;"></td>
            <td width="30%" align="center" style="border-bottom: none; font-size: 12px;">расшифровка</td>
        </tr>


    </table>
</div>
</body>
</html>
