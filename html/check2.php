<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style type="text/css">
        * {font-family: arial;font-size: 14px; line-height: 14px;}
        table {margin: 0 0 20px 0;width: 100%;border-collapse: collapse;border-spacing: 0;}
        table th {padding: 5px;font-weight: bold;}
        table td {padding: 5px;}
        .header {font-size: 12px;line-height: 12px;text-align: center;}
        .header td{ padding: 15px}
        h1 {margin: 0 0 10px 0;padding: 10px 0;border-bottom: 2px solid #000;font-weight: bold;font-size: 20px;}

        /* Реквизиты банка */
        .details td {padding: 5px;border: 2px solid #000000;font-size: 14px;line-height: 12px;vertical-align: top;}

        /* Поставщик/Покупатель */
        .contract th {padding: 3px 0;vertical-align: top;text-align: left;font-size: 13px;line-height: 15px;}
        .contract td {padding: 3px 0;}

        /* Наименование товара, работ, услуг */
        .list thead, .list tbody  {border: 2px solid #000;}
        .list thead th {padding: 5px;border: 1px solid #000;vertical-align: middle;text-align: center;}
        .list tbody td {padding: 5px;border: 1px solid #000;vertical-align: middle;font-size: 14px;line-height: 14px;}
        .list tfoot th {padding: 3px 5px;border: none;text-align: right;}

        /* Сумма */
        .total {margin: 0 0 20px 0;padding: 0 0 10px 0;border-bottom: 2px solid #000;}
        .total p {margin: 0;padding: 0;}

        /* Руководитель, бухгалтер */
        .sign {position: relative;}
        .sign table {width: 90%;}
        .sign th {}
        .sign td {border-bottom: 1px solid #000;font-size: 12px;}
    </style>
</head>
<body>

<table class="header">
    <tr>
        <td>
            Внимание! Счет действителен в течении двух дней.<br/>
            Оплата данного счета означает согласие с условиями поставки товара.<br/>
            Уведомление об оплате обязательно, в противном случае не гарантируется<br/>
            наличие товара на складе. Товар отпускается по факту прихода денег<br/>
            на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.<br/>
        </td>
    </tr>
</table>

<table class="details">
    <tr>
        <td colspan="2" style="border-bottom: none;">Брянское отделение №8605 ПАО Сбербанк</td>
        <td>БИК</td>
        <td colspan="3" style="border-bottom: none;">041501601</td>
    </tr>
    <tr>
        <td colspan="2" style="border-top: none; font-size: 12px;">Банк получателя</td>
        <td>Сч. № </td>
        <td colspan="3" style="border-bottom: none;">30101810400000000601</td>
    </tr>
    <tr>
        <td width="25%">ИНН 323401752808</td>
        <td width="30%">ОГРН 316325600076797</td>
        <td width="10%">Сч. №</td>
        <td colspan="3">40802810608000004851</td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2" style="border-bottom: none;">ИП Архипченко Павел Николаевич</td>
        <td>Вид оп.</td>
        <td rowspan="2" width="10%"></td>
        <td width="10%" >Срок плат</td>
        <td rowspan="3"></td>
    </tr>
    <tr>
        <td>Наз. пл</td>
        <td width="10%">Очер. плат</td>
    </tr>
    <tr>
        <td colspan="2" style="border-top: none; font-size: 12px;">Банк получателя</td>
        <td>Код</td>
        <td width="10%" style="line-height: 16px;"></td>
        <td width="10%">Рез. поле</td>
    </tr>
</table>


<h1>Счет на оплату № UT-36148 от 20 ноября 2023 г.</h1>

<table class="contract">
    <tbody>
    <tr>
        <td width="15%">Поставщик:</td>
        <th width="85%">ИП Архипченко Павел Николаевич, ИНН 323401752808, ОГРН 316325600076797, 241037, Брянская обл. г. Брянск ул. Авиационная д. 5 кв. 48</th>
    </tr>
    <tr>
        <td>Покупатель:</td>
        <th>ООО "ОСПАЗ", ИНН 5720022487, КПП 572001001, 302038, Орловская обл, м.о. Орловский, ул Раздольная, стр. 105Л</th>
    </tr>
    </tbody>
</table>

<table class="list">
    <thead>
    <tr>
        <th width="5%">№</th>
        <th width="5%">Артикул</th>
        <th width="54%">Товары (работы, услуги) </th>
        <th width="13%" colspan="2">Количество</th>
        <th width="14%">Цена</th>
        <th width="14%">Сумма</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td align="left">1</td>
        <td align="left">66282</td>
        <td align="left">3ПКВТп-10-70/120 Концевая кабельная муфта внутренней установки для кабелей с изоляцией из сшитого по</td>
        <td align="right">2</td>
        <td align="right">шт</td>
        <td align="right">2 811,60</td>
        <td align="right">5 623,20</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="6">Итого:</th>
        <th>5 623,20</th>
    </tr>
    </tfoot>
</table>

<div class="total">
    <p>Всего наименований 1, на сумму 6 747,84 руб. руб.</p>
    <p><strong>Шесть тысяч семьсот сорок семь рублей 84 копейки</strong></p>
</div>

<div class="sign">
    <table width="100%">
        <tr>
            <th rowspan="2"align="left" style="vertical-align: top; padding-top: 20px;">Директор</th>
            <td width="40%" align="center">
                <span style="display: inline-block; position: relative;">
                    <img style="display: inline-block; position: absolute; bottom: -30px; left: -80px;" width="70px" src="/f/i/signature/arhipchenko.png">
                    <img style="display: inline-block; position: absolute; bottom: -120px; left: -125px;" width="200px" src="/f/i/signature/arhipchenko-stamp.png">
                </span>
            </td>
            <td width="5%" style="border-bottom: none;"></td>
            <td width="40%" align="center" style="vertical-align: top; padding-top: 20px;"><strong>Архипченко П.Н.</strong></td>
        </tr>
        <tr>
            <td  width="40%" align="center" style="border-bottom: none; font-size: 12px;">подпись</td>
            <td width="5%" style="border-bottom: none;"></td>
            <td width="40%" align="center" style="border-bottom: none; font-size: 12px;">расшифровка подписи</td>
        </tr>

        <tr>
            <th rowspan="2"align="left" style="vertical-align: top; padding-top: 30px;">Бухгалтер</th>
            <td width="40%" align="center">
                 <span style="display: inline-block; position: relative;">
                    <img style="display: inline-block; position: absolute; bottom: -50px; left: -60px;" width="70px" src="/f/i/signature/arhipchenko.png">
                </span>
            </td>
            <td width="5%" style="border-bottom: none;"></td>
            <td width="40%" align="center" style="vertical-align: top; padding-top: 30px;"><strong>Архипченко П.Н.</strong></td>
        </tr>
        <tr>
            <td  width="40%" align="center" style="border-bottom: none; font-size: 12px;">подпись</td>
            <td width="5%" style="border-bottom: none;"></td>
            <td width="40%" align="center" style="border-bottom: none; font-size: 12px;">расшифровка подписи</td>
        </tr>
    </table>
</div>
</body>
</html>
