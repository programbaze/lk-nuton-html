<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Акты сверки</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Акты сверки</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Акты сверки</h1>
                    </div>
                  </div>


                    <div class="acts">
                        <div class="acts__inner">



                            <div class="acts__period">
                                <div class="acts__period-title">Выбор периода</div>
                                <div class="acts__period-calendar">
                                    <div class="acts__period-date">c <input class="form-control acts__input" type="date" name="date"></div>
                                    <div class="acts__period-date">по <input class="form-control acts__input" type="date" name="date"></div>
                                </div>

                                <div class="acts__period-radio">
                                    <div class="btn-group" role="group" aria-label="Базовая группа переключателей радио">
                                        <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
                                        <label class="btn btn-outline-primary" for="btnradio1">День</label>

                                        <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
                                        <label class="btn btn-outline-primary" for="btnradio2">Неделя</label>

                                        <input type="radio" class="btn-check" name="btnradio" id="btnradio3" autocomplete="off">
                                        <label class="btn btn-outline-primary" for="btnradio3">Месяц</label>

                                        <input type="radio" class="btn-check" name="btnradio" id="btnradio4" autocomplete="off">
                                        <label class="btn btn-outline-primary" for="btnradio4">Квартал</label>

                                        <input type="radio" class="btn-check" name="btnradio" id="btnradio5" autocomplete="off">
                                        <label class="btn btn-outline-primary" for="btnradio5">Год</label>
                                    </div>
                                </div>

                                <div class="acts__period-btn">
                                    <div class="red-btn footer__btn">Сформировать отчет</div>
                                </div>
                            </div>


                            <div class="acts-table">
                                <div class="acts-table__row acts-table__row_head">
                                    <div class="acts-table__coll"></div>
                                    <div class="acts-table__coll">Номер акта</div>
                                    <div class="acts-table__coll">Период сверки</div>
                                </div>
                                <div class="acts-table__row">
                                    <div class="acts-table__coll">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" checked>
                                        </div>
                                    </div>
                                    <div class="acts-table__coll">20200-000212787 от&nbsp;2021-07-19</div>
                                    <div class="acts-table__coll">2021-04-01 - 2021-06-30</div>
                                </div>
                                <div class="acts-table__row">
                                    <div class="acts-table__coll">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="">
                                        </div>
                                    </div>
                                    <div class="acts-table__coll">20200-000212787 от&nbsp;2021-07-19</div>
                                    <div class="acts-table__coll">2021-04-01 - 2021-06-30</div>
                                </div>
                                <div class="acts-table__row">
                                    <div class="acts-table__coll">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="">
                                        </div>
                                    </div>
                                    <div class="acts-table__coll">20200-000212787 от&nbsp;2021-07-19</div>
                                    <div class="acts-table__coll">2021-04-01 - 2021-06-30</div>
                                </div>
                            </div>

                            <div class="acts__setting">
                                <div class="img-btn img-btn_check acts__setting-btn">Добавить счет</div>
                                <div class="img-btn img-btn_print acts__setting-btn">Печать</div>
                                <div class="img-btn img-btn_save acts__setting-btn">Скачать</div>
                            </div>

                        </div>
                    </div>





                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
