<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Профиль</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">


</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Профиль</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Профиль</h1>
                    </div>
                  </div>


                  <div class="profile-card">
                    <div class="profile-card__inner">
                      <div class="profile-card__company">
                        <div class="profile-card__logo" style="background-image: url('/f/i/logo.svg')"></div>
                        <div class="profile-card__head">
                          <div class="profile-card__name">ООО «Электрострой»</div>
                          <div class="profile-card__btns">
                            <a href="#" class="red-img-btn red-img-btn_mail">Управление рассылкой</a>
                            <a href="#" class="red-img-btn red-img-btn_profile">Изменить личные данные</a>
                          </div>
                        </div>

                        <div class="profile-card__info">

                          <div class="unit-info profile-card__unit-info">
                            <div class="unit-info__title">Должность:</div>
                            <div class="unit-info__text">Руководитель (администратор)</div>
                          </div>

                          <div class="unit-info profile-card__unit-info">
                            <div class="unit-info__title">Телефон:</div>
                            <div class="unit-info__text">+7 (4832) 400-100 доб 204</div>
                          </div>

                          <div class="unit-info profile-card__unit-info">
                            <div class="unit-info__title">E-mail:</div>
                            <div class="unit-info__text">info@nuton-electro.ru</div>
                          </div>

                          <div class="unit-info profile-card__unit-info">
                            <div class="unit-info__title">Адрес:</div>
                            <div class="unit-info__text">Брянская область д. Добрунь, ул. С.А. Халаева, 74</div>
                          </div>

                        </div>

                      </div>
                      <div class="profile-card__calculation">
                        <div class="profile-card__calculation-coll profile-card__head-title">
                          <div class="profile-card__calculation-title">Взаиморасчёт</div>
                        </div>
                        <div class="profile-card__calculation-coll">
                          <div class="profile-card__calculation-title">Сумма расрочки</div>
                          <div class="profile-card__calculation-text price-rub">10 000000</div>
                        </div>
                          <div class="profile-card__calculation-coll">
                              <div class="profile-card__calculation-title">Дебиторская задолженность:</div>
                              <div class="profile-card__calculation-text price-rub">1 256 356</div>
                          </div>
                        <div class="profile-card__calculation-coll">
                          <div class="profile-card__calculation-title">Остаток рассрочки:</div>
                          <div class="profile-card__calculation-text price-rub">8 743 356</div>
                        </div>
                        <div class="profile-card__calculation-coll">
                          <div class="profile-card__calculation-title">Ваша персональная скидка:</div>
                          <div class="profile-card__calculation-text">20%</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="profile-members">
                    <div class="profile-members__inner">

                      <div class="employees profile-members__employees">
                        <div class="employees__inner">
                          <div class="employees__title">Мои сотрудники</div>
                          <div class="employees__item">
                            <div class="employees__btns">
                              <a href="#" class="btn-lock"></a>
                              <a href="#" class="btn-trash"></a>
                            </div>
                            <div class="employees__item-name">Константинопльский Константин Николевич </div>
                            <div class="employees__item-job">Руководитель (администратор)</div>
                            <div class="employees__item-login">Логин: konstantin_electrostoi</div>
                          </div>
                          <div class="employees__item">
                            <div class="employees__btns">
                              <a href="#" class="btn-lock"></a>
                              <a href="#" class="btn-trash"></a>
                            </div>
                            <div class="employees__item-name">Константинопльский Константин Николевич </div>
                            <div class="employees__item-job">Руководитель (администратор)</div>
                            <div class="employees__item-login">Логин: konstantin_electrostoi</div>
                          </div>
                        </div>
                      </div>

                      <div class="manager profile-members__manager">
                        <div class="manager__inner">
                          <div class="manager__title">Ваш персональный менеджер</div>
                          <div class="manager__content">
                            <div class="manager__logo" style="background-image: url('/f/i/manager.jpg')"></div>
                            <div class="manager__name">Иван Иванович Кравченко</div>
                            <div class="manager__phone">+7 (4832) 400-100 Доб. 241</div>
                            <div class="manager__phone">89043624526</div>
                            <div class="manager__email">i.kravchenko@nuton-electro.ru</div>
                            <a href="#" class="red-img-btn red-img-btn_chat manager__btn">Написать сообщение</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
