<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Каталог</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Каталог</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Каталог</h1>
                    </div>
                  </div>

                  <div class="catalog">
                    <div class="catalog__inner">

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/lamps.jpg')"></div>
                        <div class="catalog__title">Источники света (лампы)</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Автомобильные лампы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лента</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Галогеновая лампа</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Люминесцентная лампа</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Филаментная лампа</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа ДРЛ, ДНАТ, МТЛ, ЛРВ</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа инфракрасная</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Энергосберегающие лампы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link"> Бактериальная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Фитолампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Новогодние</a></li>
                            <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа накаливания</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/illuminator.jpg')"></div>
                        <div class="catalog__title">Светильники</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Настольные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Управляемые</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Влагозащитные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Офисные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Прожекторы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Трековое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Детские</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Садово-парковое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Down Light</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Модульные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Промышленные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бани и сауны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Патроны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Уличные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бытовые светильники</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/electrical.jpg')"></div>
                        <div class="catalog__title">Электроустановочные изделия</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Колодки)</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Комбинированные блоки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Вилки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Компоненты и аксессуары</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Силовые разъёмы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Выключатели</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Штепсельный разъем</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки информационные, телефон, тв</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Различные устройства</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Регуляторы</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/wires.jpg')"></div>
                        <div class="catalog__title">Провода и кабели</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Автомобильные лампы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лента</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа накаливания</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Галогеновая лампаа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Люминесцентная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Филаментная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link"> Бактериальная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Фитолампа (для растений)</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа ДРЛ, ДНАТ, МТЛ, ЛРВ</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа инфракрасная</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Новогодние</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Энергосберегающие лампы</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/lightning.jpg')"></div>
                        <div class="catalog__title">Молниезащита</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Настольные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Управляемые</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Влагозащитные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Офисные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Прожекторы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Трековое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Детские</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Садово-парковое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Down Light</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Модульные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Промышленные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бани и сауны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Патроны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Уличные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бытовые светильники</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/protection.jpg')"></div>
                        <div class="catalog__title">Устройства защиты</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Колодки)</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Комбинированные блоки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Вилки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Компоненты и аксессуары</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Силовые разъёмы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Выключатели</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Штепсельный разъем</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки информационные, телефон, тв</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Различные устройства</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Регуляторы</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/shield.jpg')"></div>
                        <div class="catalog__title">Щитовое оборудование</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Автомобильные лампы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лента</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа накаливания</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Галогеновая лампаа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Люминесцентная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Светодиодная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Филаментная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link"> Бактериальная лампа</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Фитолампа (для растений)</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа ДРЛ, ДНАТ, МТЛ, ЛРВ</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Лампа инфракрасная</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Новогодние</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Энергосберегающие лампы</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/corrugation.jpg')"></div>
                        <div class="catalog__title">Гофра, кабель-канал, лотки, муфты</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Настольные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Управляемые</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Влагозащитные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Офисные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Прожекторы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Трековое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Детские</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Садово-парковое освещение</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Down Light</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Модульные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Промышленные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бани и сауны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Патроны</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Уличные</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Бытовые светильники</a></li>
                        </ul>
                      </div>

                      <div class="catalog__section">
                        <div class="catalog__logo" style="background-image: url('/f/i/catalog/lamps.jpg')"></div>
                        <div class="catalog__title">Счётчики электроэнергии</div>
                        <ul class="catalog__list">
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Колодки)</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Комбинированные блоки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Вилки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Компоненты и аксессуары</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Силовые разъёмы</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Выключатели</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Штепсельный разъем</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Розетки информационные, телефон, тв</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Различные устройства</a></li>
                          <li class="catalog__list-li"><a href="#" class="catalog__link">Регуляторы</a></li>
                        </ul>
                      </div>


                    </div>

                  </div>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
