<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Главная</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Каталог</a></li>
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Источники света</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Светодиодные лампы</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Светодиодная лампа IN HOME A60 8Вт 230В 6500К Е27 720Лм</h1>
                    </div>
                  </div>

                  <div class="cart">
                    <div class="cart__inner">

                      <div class="cart__buy">

                        <div class="tobasket">
                          <div class="tobasket__inner">
                            <div class="tobasket__wrap-amount">
                              <div class="amount tobasket__amount">
                                <div class="amount__inner">
                                  <button class="btn-plus btn-plus_square"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus btn-minus_square"></button>
                                </div>
                              </div>
                              <div class="tobasket__price">
                                <div class="price-rub">51.90</div>
                              </div>
                            </div>
                            <div class="tobasket__btns">
                              <button class="btn-tobaske">Добавить в корзину</button>
                              <button class="btn-heart btn-heart_square"></button>
                            </div>
                          </div>
                        </div>

                        <div class="download-list">
                          <div class="download-list__inner">
                            <div class="download-list__title">Дополнительный материал производителя</div>
                            <a href="#" class="download-list__link">Сертификат</a>
                            <a href="#" class="download-list__link">Паспорт</a>
                            <a href="#" class="download-list__link">Руководство по эксплутации</a>
                          </div>
                        </div>

                      </div>

                      <div class="cart__images">

                        <div class="slider-cart">
                          <div class="slider-cart__inner js__slider-cart">
                            <img src="/f/i/slider-cart/1.jpg" alt="Название товара" class="slider-cart__img">
                            <img src="/f/i/slider-cart/2.png" alt="Название товара" class="slider-cart__img">
                            <img src="/f/i/slider-cart/3.png" alt="Название товара" class="slider-cart__img">
                          </div>
                        </div>

                      </div>
                      <div class="cart__leftovers">

                        <div class="leftovers">
                          <div class="leftovers__title">Остатки</div>
                          <table class="leftovers__table">
                            <thead>
                            <tr>
                              <th>Склад</th>
                              <th class="leftovers__tx-right">Остаток</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                              <td>Склад Ньютон-электро Брянск</td>
                              <td class="leftovers__tx-right">120</td>
                            </tr>
                            <tr>
                              <td>Склад Ньютон-электро Калуга</td>
                              <td class="leftovers__tx-right">1</td>
                            </tr>
                            <tr>
                              <td>Склад Ньютон-электро Смоленск</td>
                              <td class="leftovers__tx-right">146</td>
                            </tr>
                            <tr>
                              <td>Склад Ньютон-электро Тула</td>
                              <td class="leftovers__tx-right">0</td>
                            </tr>
                            <tr>
                              <td>Склад производителя IEK</td>
                              <td class="leftovers__tx-right">1357</td>
                            </tr>
                            <tr>
                              <td>Склад производителя Shnaider</td>
                              <td class="leftovers__tx-right">14</td>
                            </tr>
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="characteristics">
                    <div class="characteristics__inner">
                      <div class="characteristics__title">Характеристики товара:</div>
                      <div class="characteristics__list">
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Класс:</div>
                          <div class="characteristics__item-desc">Светильники для внутреннего освещения</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Страна:</div>
                          <div class="characteristics__item-desc">Китай</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Производитель:</div>
                          <div class="characteristics__item-desc">FERON</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Артикул:</div>
                          <div class="characteristics__item-desc">DL13 хром</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Артикул расширенный:</div>
                          <div class="characteristics__item-desc">15129</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Ед.измерения:</div>
                          <div class="characteristics__item-desc">шт</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Наименование в прайсе производителя:</div>
                          <div class="characteristics__item-desc">Светильник потолочный встраиваемый, MR16 G5.3 хром, DL13</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Тип изделия:</div>
                          <div class="characteristics__item-desc">Светильник</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Тип лампы:</div>
                          <div class="characteristics__item-desc">КГМ</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Класс:</div>
                          <div class="characteristics__item-desc">Светильники для внутреннего освещения</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Страна:</div>
                          <div class="characteristics__item-desc">Китай</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Производитель:</div>
                          <div class="characteristics__item-desc">FERON</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Артикул:</div>
                          <div class="characteristics__item-desc">DL13 хром</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Артикул расширенный:</div>
                          <div class="characteristics__item-desc">15129</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Ед.измерения:</div>
                          <div class="characteristics__item-desc">шт</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Наименование в прайсе производителя:</div>
                          <div class="characteristics__item-desc">Светильник потолочный встраиваемый, MR16 G5.3 хром, DL13</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Тип изделия:</div>
                          <div class="characteristics__item-desc">Светильник</div>
                        </div>
                        <div class="characteristics__item">
                          <div class="characteristics__item-title">Тип лампы:</div>
                          <div class="characteristics__item-desc">КГМ</div>
                        </div>
                      </div>
                    </div>
                  </div>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
