<footer class="footer-platform">
    <div class="footer__inner">
        <div class="footer__col-logo">
            <div class="footer__logo-copy">
                <div class="footer__logo"></div>
                <div class="footer__copy">©&nbsp;Все&nbsp;права&nbsp;защищены ГК&nbsp;Ньютон&nbsp;электро&nbsp;2022</div>
            </div>
            <div class="footer__wrap-social">
                <a href="#" class="footer__item-social footer__item-social_fb"></a>
                <a href="#" class="footer__item-social footer__item-social_vk"></a>
                <a href="#" class="footer__item-social footer__item-social_yt"></a>
            </div>
        </div>
        <div class="footer__col-link">
            <a href="#" class="footer__link">Офисы&nbsp;компании</a>
            <a href="#" class="footer__link">Юридические&nbsp;данные</a>
            <a href="#" class="footer__link">О&nbsp;нас</a>
            <a href="#" class="footer__link">Новости</a>
            <a href="#" class="footer__link">Акции</a>
            <a href="#" class="footer__link">Производители</a>
            <a href="#" class="footer__link footer__link_phone">Избранное</a>
        </div>
        <div class="footer__col-phone">
            <div class="footer__phones">
                <a href="tel:8-800-888-88-88" class="footer__item-phone" itemprop="phone">8-800-888-88-88</a>
                <a href="tel:+7(4832)400-200" class="footer__item-phone" itemprop="phone">+7(4832)400-200</a>
            </div>
            <div class="red-btn footer__btn">Обратная связь</div>
            <a href="mailto:info@nuton-electro.ru" class="footer__email" itemprop="email">info@nuton-electro.ru</a>
        </div>
    </div>
</footer>
