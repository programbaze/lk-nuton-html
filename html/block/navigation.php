<div class="navigation js__navigation">
    <div class="navigation__inner">
        <div class="navigation__item navigation__item_switch js__navigation-switch"><span class="navigation__text">Свернуть</span></div>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/light-source.svg">
            <span class="navigation__text">Источники свет</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/lamp.svg">
            <span class="navigation__text">Светильники</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/wiring-accessories.svg">
            <span class="navigation__text">Электроустановочные изделия</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/wires.svg">
            <span class="navigation__text">17. Охранно-пожарная сигнализация/Противопожарное оборудование</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/lightning-protection.svg">
            <span class="navigation__text">18. Садовые принадлежности/Товары для дома/Хобби</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/protection-devices.svg">
            <span class="navigation__text">Устройства защиты</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/panel-equipment.svg">
            <span class="navigation__text">Щитовое оборудование</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/corrugation.svg">
            <span class="navigation__text">Гофра, кабель-канал, лотки, муфты</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/mounting-box.svg">
            <span class="navigation__text">Коробки монтажные, подрозетники</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/electricity-meter.svg">
            <span class="navigation__text">Счётчики электроэнергии</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/tool.svg">
            <span class="navigation__text">Ручной инструмент</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/electro-tool.svg">
            <span class="navigation__text">Электроинструмент</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/flashlight.svg">
            <span class="navigation__text">Фонари и элементы питания</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/extension.svg">
            <span class="navigation__text">Удлинители и стабилизаторы</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/heating-equipment.svg">
            <span class="navigation__text">Отопительное оборудование</span>
        </a>
        <a href="#" class="navigation__item">
            <img class="navigation__img" src="/f/i/icons/nav/generator.svg">
            <span class="navigation__text">Генераторы и двигатели</span>
        </a>
    </div>
</div>
