<header class="header">
    <div class="city-logo">
        <div class="city-logo__inner">
            <a href="#" class="logo"><img src="/f/i/logo.svg" class="logo__img" alt="nuton"></a>
            <div class="choice-city">
                <div class="choice-city__opted">Москва</div>
            </div>
        </div>
    </div>
    <div class="search-wishlist">
        <div class="search-wishlist__inner">
            <div class="search">
                <form class="search__inner">
                    <input type="text" class="search__input" placeholder="Поиск товаров по базе">
                    <button type="submit" class="search__magnifier"></button>
                </form>
            </div>
            <div class="wishlist">
                <div class="wishlist__inner">
                    <a href="" class="wishlist__item wishlist__item_simple"></a>
                    <a href="" class="wishlist__item wishlist__item_heart">
                        <span class="wishlist__sum">3</span>
                    </a>
                    <a href="" class="wishlist__item wishlist__item_user"></a>
                    <a href="" class="wishlist__item wishlist__item_basket">
                        <span class="wishlist__sum">12</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
