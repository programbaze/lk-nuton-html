<div class="mob-wishlist">
    <div class="mob-wishlist__inner">
        <a href="#" class="mob-wishlist__item mob-wishlist__item_home">Главная</a>
        <div class="mob-wishlist__item mob-wishlist__item_catalog js__navigation-switch">Каталог</div>
        <a href="#" class="mob-wishlist__item mob-wishlist__item_basket">Корзина</a>
        <a href="#" class="mob-wishlist__item mob-wishlist__item_cargo">Отгрузки</a>
        <div class="mob-wishlist__item mob-wishlist__item_user" data-popup-btn="profile">Профиль</div>
    </div>
</div>
