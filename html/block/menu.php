<section class="menu">
    <div class="menu__inner">
        <a href="#" class="menu__item-link">Офисы продаж</a>
        <a href="#" class="menu__item-link">Отгрузки</a>
        <a href="#" class="menu__item-link">Поддержка</a>
        <a href="#" class="menu__item-link">Статистика</a>
        <a href="#" class="menu__item-link">Акты&nbsp;сверки</a>
        <a href="#" class="menu__item-link">Акции</a>
    </div>
</section>
