<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Корзина</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Корзина</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Корзина</h1>
                    </div>
                  </div>

                  <div class="about-orders">
                    <div class="about-orders__inner">

                      <div class="about-orders__item">
                        <div class="about-orders__title about-orders__title_pos">84</div>
                        <div class="about-orders__subtitle">позиции</div>
                      </div>

                      <div class="about-orders__item">
                        <div class="about-orders__title"><div class="price-rub">86 823</div></div>
                        <div class="about-orders__subtitle">сумма заказа</div>
                      </div>

                      <div class="about-orders__item">
                        <div class="about-orders__title">3-4 дня</div>
                        <div class="about-orders__subtitle">срок поставки</div>
                      </div>

                    </div>
                  </div>

                  <div class="control-buy">
                    <div class="control-buy__inner">
                      <div class="control-buy__left">
                        <a href="#" class="red-btn">Продолжить покупку</a>
                        <a href="#" class="red-btn">Добавить по артикулам</a>
                      </div>
                      <div class="control-buy__right">
                        <a href="#" class="red-btn red-btn_spinner">Оформить заказ</a>
<!--                        <a href="#" class="img-btn img-btn_check">Добавить счет</a>-->
<!--                        <a href="#" class="img-btn img-btn_print">Печать</a>-->
<!--                        <a href="#" class="img-btn img-btn_save">Скачать</a>-->
                      </div>
                    </div>
                  </div>



                  <div class="choice">
                    <div class="choice__inner">

                      <div class='product'>
                        <table class='product__table'>

                          <tr class='product__head product__row'>
                            <td class='product__head-col product__col product__col_tx-center'>Фото</td>
                            <td class='product__head-col product__col'>Артикул</td>
                            <td class='product__head-col product__col'>Наименование <span class='product__sorting product__sorting_active'></span></td>
                            <td class='product__head-col product__col'>Бренд <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Доступность <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Упаковка <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Ваша цена <span class='product__sorting'></span></td>
                            <td class='product__head-col product__col'>Количество</td>
                            <td class='product__head-col product__col'></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="999">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                    <input type="text" class="amount__coll" value="999">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/1.jpg'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/2.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                          <tr class='product__row'>
                            <td class='product__col'>
                              <img class='product__img' src='/f/i/slider-cart/3.png'>
                            </td>
                            <td class='product__col'>N23589GT2</td>
                            <td class='product__col'>Лампочка Gauss LED 10вт Е27 белый (LB-92)</td>
                            <td class='product__col'>Gauss</td>
                            <td class='product__col'>648 шт.</td>
                            <td class='product__col'>100шт.</td>
                            <td class='product__col'>52 р.</td>
                            <td class='product__col'>
                              <div class="amount">
                                <div class="amount__inner">
                                  <button class="btn-plus"></button>
                                  <input type="text" class="amount__coll" value="99">
                                  <button class="btn-minus"></button>
                                </div>
                              </div>
                            </td>
                            <td class='product__col product__tx-end'><span class='btn-trash'></span></td>
                          </tr>

                        </table>


                          <!-- Adaptive block product (show 1319px)-->
                          <div class="prod-small">
                              <div class="prod-small__inner">
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт.</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-plus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-minus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-trash"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/1.jpg">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт.</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-plus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-minus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-trash"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт.</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-plus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-minus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-trash"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/1.jpg">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт.</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-plus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-minus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-trash"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                                  <!--  Item product-->
                                  <div class="prod-small__item">
                                      <div class="prod-small__left">
                                          <div class="prod-small__wrap-img">
                                              <img class="prod-small__img" src="/f/i/slider-cart/2.png">
                                          </div>
                                          <div class="prod-small__wrap-heart">
                                              <div class="btn-heart active"></div>
                                          </div>
                                      </div>
                                      <div class="prod-small__right">
                                          <div class="prod-small__title">Лампочка Gauss LED 10вт Е27 белый (LB-92)</div>
                                          <div class="prod-small__characteristic">
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Артикул</div>
                                                  <div class="prod-small__characteristic-value">N23589GT2</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Бренд</div>
                                                  <div class="prod-small__characteristic-value">Gauss</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Доступность</div>
                                                  <div class="prod-small__characteristic-value">648 шт.</div>
                                              </div>
                                              <div class="prod-small__item-characteristic">
                                                  <div class="prod-small__characteristic-name">Упаковка</div>
                                                  <div class="prod-small__characteristic-value">100шт.</div>
                                              </div>
                                          </div>
                                          <div class="prod-small__tobasket">
                                              <div class="prod-small__price">52 р.</div>
                                              <div class="prod-small__amount-cart">
                                                  <div class="prod-small__wrap-amount">
                                                      <div class="amount">
                                                          <div class="amount__inner">
                                                              <button class="btn-plus"></button>
                                                              <input type="text" class="amount__coll" value="99">
                                                              <button class="btn-minus"></button>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="prod-small__wrap-cart">
                                                      <div class="btn-trash"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <!--  // Item product-->
                              </div>
                          </div>
                          <!-- // Adaptive block product (show 1319px)-->


                      </div>


                    </div>
                  </div>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
