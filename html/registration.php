<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Регистрация</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <main class="main">

          <div class="enter">
            <div class="enter__inner">

              <form method="POST" class="needs-validation" action="">
                <div class="form-group mb-4"><h4>Регистрация в личном кабинете <nobr>ГК "Ньютон электро"</nobr></h4></div>

                <div class="form-group mb-3"><h5>Юридические данные</h5></div>

                <div class="form-group mb-3">
                  <label for="nameorg">Наименование организации</label>
                  <input id="nameorg" type="" class="form-control">
                    <div class="invalid-feedback" style="display: block">
                        Поле обязательное для заполнения
                    </div>
                </div>

                <div class="row form-group mb-4">

                  <div class="col-md-6">
                    <label for="inn">ИНН</label>
                    <input id="inn" type="" class="form-control invalid">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                  <div class="col-md-6">
                    <label for="kpp">КПП</label>
                    <input id="kpp" type="" class="form-control invalid">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                </div>

                <div class="form-group mb-3"><h5>Контактные данные</h5></div>

                <div class="form-group mb-3">
                  <label for="fio">Ф.И.О. *(контактного лица)</label>
                  <input id="fio" type="" class="form-control">
                    <div class="invalid-feedback" style="display: block">
                        Поле обязательное для заполнения
                    </div>
                </div>

                <div class="row form-group mb-3">

                  <div class="col-md-6">
                    <label for="phone">Телефон</label>
                    <input id="phone" type="phone" class="form-control">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                  <div class="col-md-6">
                    <label for="email">E-mail</label>
                    <input id="email" type="email" class="form-control">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                </div>

                <div class="row form-group mb-3">

                  <div class="col-md-6">
                    <label for="city">Город</label>
                    <input id="city" type="" class="form-control">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                  <div class="col-md-6">
                    <label for="position">Должность</label>
                    <input id="position" type="" class="form-control">
                      <div class="invalid-feedback" style="display: block">
                          Поле обязательное для заполнения
                      </div>
                  </div>

                </div>

                <div class="form-group mb-3">
                  <div class="form-check mb-3">
                    <input class="form-check-input invalid" type="checkbox" name="" id="personal">
                    <label class="form-check-label" for="personal">
                      Даю согласие на обработку персональных данных, согласно Политике безопасности
                    </label>

                  </div>
                </div>

                <div class="form-group mb-3">
                  <button type="submit" class="red-btn col-md-12">
                    Зарегистрироваться
                  </button>
                </div>

              </form>

            </div>

          </div>

        </main>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>
</body>
</html>
