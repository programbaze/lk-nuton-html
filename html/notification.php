<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Уведомления</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Уведомления</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Ваши уведомления</h1>
                    </div>
                  </div>

                    <div class="notification">
                        <div class="notification__inner">

                            <div class="notification__item notification__item_entry">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Успешный вход на сайт 01.01.21</div>
                                    <div class="notification__text">Вы вошли в свой аккаунт 01.01.21 в 15:43 с устройства ...................................</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_new">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Новый заказ</div>
                                    <div class="notification__text">По адресу .......................................... был успешно сформирован заказ и готов к самовывозу</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_equipment">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Ваш заказ в комплектации</div>
                                    <div class="notification__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, beatae!</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_staffed">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Ваш заказ укамплектован</div>
                                    <div class="notification__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, beatae!</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_delivery">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Ваш заказ в доставке</div>
                                    <div class="notification__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, beatae!</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_done">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Ваш заказ выполнен</div>
                                    <div class="notification__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, beatae!</div>
                                </div>
                            </div>

                            <div class="notification__item notification__item_canceled">
                                <div class="notification__left"></div>
                                <div class="notification__right">
                                    <div class="notification__title">Ваш заказ отменен</div>
                                    <div class="notification__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, beatae!</div>
                                </div>
                            </div>

                        </div>
                    </div>



                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
