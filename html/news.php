<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Новости</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Новости</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Новости</h1>
                    </div>
                  </div>

                    <section class="news news_page">
                        <div class="news__inner">

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">NYT: Россия издевается над запретами Олимпиады</a>
                                    <div class="item-news__preface">Американская газета The New York Times посвятила статью российским спортсменам,
                                        выступающим на Олимпиаде в Токио без национального флага и гимна, и пришла к выводу, что россиян не...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Новый алгоритм управляет дронами быстрее гоночных пилотов</a>
                                    <div class="item-news__preface">Специалисты холдинга «Швабе» разработали голограммный оптический элемент
                                        на конической поверхности, который позволит измерять форму главных зеркал телескопов.
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Будущее уже сейчас: обзор цифровых технологий в стоматологии</a>
                                    <div class="item-news__preface">Цифровая стоматология является инновационным направлением в медицине, где широко используется компьютерное оборудование.
                                        Основная цель цифровой стоматологии – комфорт во время...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">NYT: Россия издевается над запретами Олимпиады</a>
                                    <div class="item-news__preface">Американская газета The New York Times посвятила статью российским спортсменам,
                                        выступающим на Олимпиаде в Токио без национального флага и гимна, и пришла к выводу, что россиян не...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Новый алгоритм управляет дронами быстрее гоночных пилотов</a>
                                    <div class="item-news__preface">Специалисты холдинга «Швабе» разработали голограммный оптический элемент
                                        на конической поверхности, который позволит измерять форму главных зеркал телескопов.
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Будущее уже сейчас: обзор цифровых технологий в стоматологии</a>
                                    <div class="item-news__preface">Цифровая стоматология является инновационным направлением в медицине, где широко используется компьютерное оборудование.
                                        Основная цель цифровой стоматологии – комфорт во время...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">NYT: Россия издевается над запретами Олимпиады</a>
                                    <div class="item-news__preface">Американская газета The New York Times посвятила статью российским спортсменам,
                                        выступающим на Олимпиаде в Токио без национального флага и гимна, и пришла к выводу, что россиян не...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Новый алгоритм управляет дронами быстрее гоночных пилотов</a>
                                    <div class="item-news__preface">Специалисты холдинга «Швабе» разработали голограммный оптический элемент
                                        на конической поверхности, который позволит измерять форму главных зеркал телескопов.
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                            <div class="item-news news__item-news">
                                <div class="item-news__inner">
                                    <a href="#" class="item-news__title">Будущее уже сейчас: обзор цифровых технологий в стоматологии</a>
                                    <div class="item-news__preface">Цифровая стоматология является инновационным направлением в медицине, где широко используется компьютерное оборудование.
                                        Основная цель цифровой стоматологии – комфорт во время...
                                    </div>
                                    <a href="#" class="btn-arrow news__btn-arrow"></a>
                                </div>
                            </div>

                        </div>
                    </section>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
