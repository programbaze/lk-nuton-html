<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Поддержка</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Поддержка</li>
                    </ul>
                  </div>

                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Поддержка</h1>
                    </div>
                  </div>


                    <div class="support">
                        <div class="support__inner">

                            <div class="search support__search">
                                <form class="search__inner">
                                    <input type="text" class="search__input" placeholder="Поиск товаров по базе">
                                    <button type="submit" class="search__magnifier"></button>
                                </form>
                            </div>

                            <div class="support__wrap-topics">

                                <div class="support__topic">
                                    <div class="support__topic-title">Технические</div>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                </div>

                                <div class="support__topic">
                                    <div class="support__topic-title">Бухгалтерия</div>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                </div>

                                <div class="support__topic">
                                    <div class="support__topic-title">Работа сайта</div>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                </div>

                                <div class="support__topic">
                                    <div class="support__topic-title">Общие</div>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                    <a href="/html/article.php" class="support__question">Почему не получается зарегистрироваться?</a>
                                    <a href="/html/article.php" class="support__question">Забыл пароль. Что делать?</a>
                                </div>

                            </div>

                            <div class="feedback">
                                <div class="feedback__title">Написать нам</div>
                                <form action="" class="feedback__form">
                                    <div class="row form-group mb-3">
                                        <div class="col-md-6">
                                            <label for="topic">Тема письма</label>
                                            <input id="topic" type="phone" class="form-control invalid">
                                            <div class="invalid-feedback" style="display: block">
                                                Поле обязательное для заполнения
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="email">E-mail</label>
                                            <input id="email" type="email" class="form-control">
                                            <div class="invalid-feedback" style="">
                                                Поле обязательное для заполнения
                                            </div>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="message">Сообщение</label>
                                            <textarea class="form-control" id="message" rows="10"></textarea>
                                            <div class="invalid-feedback" style="">
                                                Поле обязательное для заполнения
                                            </div>
                                        </div>

                                        <div class="form-group mb-3">
                                            <div class="form-check mb-3">
                                                <input class="form-check-input invalid" type="checkbox" name="" id="personal">
                                                <label class="form-check-label" for="personal">
                                                    Даю согласие на обработку персональных данных, согласно Политике безопасности
                                                </label>

                                            </div>
                                        </div>

                                        <div class="form-group mb-3">
                                            <button type="submit" class="red-btn">
                                                Отправить
                                            </button>
                                        </div>

                                    </div>
                                </form>
                            </div>



                        </div>
                    </div>




                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
