<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="/f/i/favicon32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/f/i/favicon64.png" sizes="64x64">
    <link rel="apple-touch-icon" sizes="180x180" href="/f/i/favicon180.png">

    <title>Статистика</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="/f/css/bootstrap.min.css" rel="stylesheet">
    <link href="/f/css/main.min.css" rel="stylesheet">

</head>
<body>
<div>
    <div class="wrapper">

        <?php require('../html/block/header.php'); ?>

        <main class="main">

            <?php require('../html/block/navigation.php');  ?>

            <div class="content">
                <div class="content__inner">
                    <?php  require('../html/block/menu.php'); ?>

                  <div class="breadcrumbs">
                    <ul class="breadcrumbs__list">
                      <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
                      <li class="breadcrumbs__item breadcrumbs__item_active">Статистика</li>
                    </ul>
                  </div>
                  <div class="title">
                    <div class="title__inner">
                      <h1 class="title__h1">Статистика компании ООО «Энергосервис»</h1>
                    </div>
                  </div>

                  <div class="statistics">
                    <div class="statistics__inner">
                      <nav  class="statistics__nav">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                          <button class="nav-link active" id="nav-goods-tab" data-bs-toggle="tab" data-bs-target="#nav-goods" type="button" role="tab" aria-controls="nav-goods" aria-selected="true">Товары</button>
                          <button class="nav-link" id="nav-finance-tab" data-bs-toggle="tab" data-bs-target="#nav-finance" type="button" role="tab" aria-controls="nav-finance" aria-selected="false">Финансы</button>
                          <button class="nav-link" id="nav-brands-tab" data-bs-toggle="tab" data-bs-target="#nav-brands" type="button" role="tab" aria-controls="nav-brands" aria-selected="false">Бренды</button>
                          <button class="nav-link" id="nav-delivery-tab" data-bs-toggle="tab" data-bs-target="#nav-delivery" type="button" role="tab" aria-controls="nav-delivery" aria-selected="false">Доставка</button>
                        </div>
                      </nav>
                      <div class="tab-content statistics__tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-goods" role="tabpanel" aria-labelledby="nav-goods-tab">
                            <div class="statistics__graffics">Графики (товары)</div>
                            <div class="statistics__info">
                                <div class="accordion">
                                    <div class="accordion__head">
                                        <div class="accordion__title">Отгрузка №6 (21.07.2021)</div>
                                        <div class="accordion__btn open"></div>
                                    </div>
                                    <div class="accordion__content">

                                        <div class="shipment-table">
                                            <div class="shipment-table__row shipment-table__row_head">
                                                <div class="shipment-table__coll">№</div>
                                                <div class="shipment-table__coll shipment-table__coll_name">Наименование</div>
                                                <div class="shipment-table__coll">Артикул</div>
                                                <div class="shipment-table__coll">Количество</div>
                                                <div class="shipment-table__coll">Цена</div>
                                            </div>
                                            <div class="shipment-table__row">
                                                <div class="shipment-table__coll">1</div>
                                                <div class="shipment-table__coll shipment-table__coll_name">Накладка розетка НР-1-0-ББ без заземляющего контакта BOLERO белый IEK</div>
                                                <div class="shipment-table__coll">1060шт.</div>
                                                <div class="shipment-table__coll">52 р.</div>
                                                <div class="shipment-table__coll">52 р.</div>
                                            </div>
                                            <div class="shipment-table__row">
                                                <div class="shipment-table__coll">2</div>
                                                <div class="shipment-table__coll shipment-table__coll_name">Накладка розетка НР-1-0-ББ без заземляющего контакта BOLERO белый IEK</div>
                                                <div class="shipment-table__coll">1004444шт.</div>
                                                <div class="shipment-table__coll">525 р.</div>
                                                <div class="shipment-table__coll">52 р.</div>
                                            </div>
                                            <div class="shipment-table__row">
                                                <div class="shipment-table__coll">3</div>
                                                <div class="shipment-table__coll shipment-table__coll_name">Накладка розетка НР-1-0-ББ без заземляющего контакта BOLERO белый IEK</div>
                                                <div class="shipment-table__coll">100шт.</div>
                                                <div class="shipment-table__coll">52 р.</div>
                                                <div class="shipment-table__coll">52 888 р.</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion__head">
                                        <div class="accordion__title">Отгрузка №5 (21.07.2021)</div>
                                        <div class="accordion__btn"></div>
                                    </div>
                                    <div class="accordion__content"></div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion__head">
                                        <div class="accordion__title">Отгрузка №4 (21.07.2021)</div>
                                        <div class="accordion__btn"></div>
                                    </div>
                                    <div class="accordion__content"></div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion__head">
                                        <div class="accordion__title">Отгрузка №3 (21.07.2021)</div>
                                        <div class="accordion__btn"></div>
                                    </div>
                                    <div class="accordion__content"></div>
                                </div>
                            </div>




                        </div>
                        <div class="tab-pane fade" id="nav-finance" role="tabpanel" aria-labelledby="nav-finance-tab">
                            <div class="statistics__graffics">Графики (Финансы)</div>
                            <div class="statistics__info">
                                <div class="shipment-table">
                                    <div class="shipment-table__row shipment-table__row_head">
                                        <div class="shipment-table__coll">№</div>
                                        <div class="shipment-table__coll shipment-table__coll_name">Наименование</div>
                                        <div class="shipment-table__coll">Дата</div>
                                        <div class="shipment-table__coll">Цена</div>
                                    </div>
                                    <div class="shipment-table__row">
                                        <div class="shipment-table__coll">1</div>
                                        <div class="shipment-table__coll shipment-table__coll_name">Отгрузка №6 - 4 товара</div>
                                        <div class="shipment-table__coll">21.07.2021</div>
                                        <div class="shipment-table__coll">52 р.</div>
                                    </div>
                                    <div class="shipment-table__row">
                                        <div class="shipment-table__coll">2</div>
                                        <div class="shipment-table__coll shipment-table__coll_name">Отгрузка №6 - 4 товара</div>
                                        <div class="shipment-table__coll">21.07.2021</div>
                                        <div class="shipment-table__coll">52 р.</div>
                                    </div>
                                    <div class="shipment-table__row">
                                        <div class="shipment-table__coll">3</div>
                                        <div class="shipment-table__coll shipment-table__coll_name">Отгрузка №6 - 4 товара</div>
                                        <div class="shipment-table__coll">21.07.2021</div>
                                        <div class="shipment-table__coll">52 888 р.</div>
                                    </div>
                                </div>
                            </div>
                         </div>
                        <div class="tab-pane fade" id="nav-brands" role="tabpanel" aria-labelledby="nav-brands-tab">
                            <div class="statistics__graffics">Графики (Бренды)</div>
                            <div class="statistics__info">
                                <div class="statistics-manufacturer">
                                    <h2 class="statistics-manufacturer__title">Основные поставляемые бренды</h2>
                                    <div class="manufacturer__list">
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/1.png" class="statistics-manufacturer__logo">
                                        </a>
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/2.png" class="statistics-manufacturer__logo">
                                        </a>
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/3.png" class="statistics-manufacturer__logo">
                                        </a>
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/4.png" class="statistics-manufacturer__logo">
                                        </a>
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/5.png" class="statistics-manufacturer__logo">
                                        </a>
                                        <a href="#" class="statistics-manufacturer__item">
                                            <img src="/f/i/manufacturer/6.png" class="statistics-manufacturer__logo">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-delivery" role="tabpanel" aria-labelledby="nav-delivery-tab">4 Доставка</div>
                      </div>
                    </div>

                  </div>


                    <?php require('../html/block/footer.php'); ?>
                </div>
            </div>
        </main>

        <?php require('../html/block/mob-wishlist.php'); ?>

    </div>
</div>
<script src="/f/js/jquery-3.6.1.min.js"></script>
<script src="/f/js/main.min.js"></script>
</body>
</html>
